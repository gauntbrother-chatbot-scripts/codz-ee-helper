# COD Zombies EE Helper

## Information can be found on the [Wiki](https://gitlab.com/gauntbrother-chatbot-scripts/codz-ee-helper/-/wikis/home)
## Bugs, feature requests and discussions can be posted [Here](https://gitlab.com/gauntbrother-chatbot-scripts/codz-ee-helper/-/issues)
###### You can support me by checking out [my Twitch](https://www.twitch.tv/gauntbrother) and drop a follow if you enjoy the content :)