import os
import sys
import json
sys.path.append(os.path.join(os.path.dirname(__file__), "lib")) #point at lib folder for classes / references

from StandardSettings_Module import MySettings

ScriptName = "COD Zombies EE Helper"
Website = "https://www.twitch.tv/gauntbrother"
Description = "COD Zombies EE Helper: Helps with steps that need an external source to solve."
Creator = "Gauntbrother"
Version = "1.0.0.0"


global MySettings
SettingsFile = os.path.join(os.path.dirname(__file__), "Settings\settings.json")

def Init():
    Log("start init")
    EnsureLocalDirectoryExists("Settings")
    
    Log("finish init")
    return

def Execute(data):

    if not data.IsChatMessage() or not data.IsFromTwitch():
        return

    MatchCommand(data)
    
    return


def Tick():
    return

def ReloadSettings(jsonData):
    # Execute json reloading here
    Log("Saving changes")
    MySettings(SettingsFile).__dict__ = json.loads(jsonData)
    MySettings(SettingsFile).Save(SettingsFile)
    Log("Changes successfully saved")
    return

def Unload():
    return

def ScriptToggled(state):
    return

#============================================================================================================
#   Custom general methods
#============================================================================================================

def EnsureLocalDirectoryExists(dirName):
    directory = os.path.join(os.path.dirname(__file__), dirName)
    if not os.path.exists(directory):
        os.makedirs(directory)

def SendMessage(message):
    Parent.SendStreamMessage(message)
    return
    
def Log(message):
    Parent.Log("CODZ EE", str(message))
    return

def addCooldown(command, data):
    if Parent.HasPermission(data.User,MySettings(SettingsFile).PermissionOverride, MySettings(SettingsFile).Info):
        #Log("Bypass cooldown")
        pass
    elif Parent.HasPermission(data.User,MySettings(SettingsFile).Permission,MySettings(SettingsFile).Info):
        Parent.AddCooldown(ScriptName, command, int(MySettings(SettingsFile).Cooldown * 60))
        #Log("Added cooldown")
        pass
    else:
        pass
    return

def isOnCooldown(command, data):
    if(Parent.GetCooldownDuration(ScriptName, command) > 0):
        Log(str(command) + " is still on cooldown (" + str(Parent.GetCooldownDuration(ScriptName, command)) + " seconds)")
        if(MySettings(SettingsFile).NotifyCooldown):
            notifyCooldown(command, data)
        return True
    else:
        return False

def notifyCooldown(command, data, topic):
    SendMessage("@" + data.UserName + " Command is on cooldown (" + str(Parent.GetCooldownDuration(ScriptName, command)) + " seconds)")
    return

#============================================================================================================
#   Match command
#============================================================================================================
def MatchCommand(data):
    if Parent.HasPermission(data.User,MySettings(SettingsFile).Permission,MySettings(SettingsFile).Info):

#Gitlab link
        if data.GetParam(0).lower() == MySettings(SettingsFile).Gitlab \
          and not isOnCooldown(MySettings(SettingsFile).Gitlab, data):
            Log("Gitlab link: called")
            SendMessage("https://gitlab.com/gauntbrother-chatbot-scripts/codz-ee-helper")
            addCooldown(MySettings(SettingsFile).Gitlab, data)

#Non-map specific
        #Morse code
        if data.GetParam(0).lower() == MySettings(SettingsFile).MorseCommand \
          and not isOnCooldown(MySettings(SettingsFile).MorseCommand, data):
          Morse(data)
    
#Black ops 2
    #Origins
        #Ice staff
        if data.GetParam(0).lower() == MySettings(SettingsFile).OriginsIceCommand \
        and not isOnCooldown(MySettings(SettingsFile).OriginsIceCommand, data):
            Log("Origins Ice: called")
            SendMessage("https://i.redd.it/oa3ic1xhy4u11.jpg")
            addCooldown(MySettings(SettingsFile).OriginsIceCommand, data)

        #Fire staff
        if data.GetParam(0).lower() == MySettings(SettingsFile).OriginsFireCommand \
        and not isOnCooldown(MySettings(SettingsFile).OriginsFireCommand, data):
            Log("Origins Fire: called")
            SendMessage("https://i.pinimg.com/originals/48/aa/93/48aa9347a3e22b57512be4551ea47a36.jpg")
            addCooldown(MySettings(SettingsFile).OriginsFireCommand, data)

        #Wind staff
        if data.GetParam(0).lower() == MySettings(SettingsFile).OriginsWindCommand \
        and not isOnCooldown(MySettings(SettingsFile).OriginsWindCommand, data):
            Log("Origins Wind: called")
            SendMessage("https://i.ytimg.com/vi/371svhcA7vo/maxresdefault.jpg")
            addCooldown(MySettings(SettingsFile).OriginsWindCommand, data)

        #Lightning staff
        if data.GetParam(0).lower() == MySettings(SettingsFile).OriginsLightningCommand \
        and not isOnCooldown(MySettings(SettingsFile).OriginsLightningCommand, data):
            Log("Origins Lightning: called")
            SendMessage("https://steamuserimages-a.akamaihd.net/ugc/830261018306058633/C3D7EB3E52EE3D50D6BBA68539B426C7CAE4B516/")
            addCooldown(MySettings(SettingsFile).OriginsLightningCommand, data)         
            
#Black ops 3
    #Gorod
        #Gorod valve
        if data.GetParam(0).lower() == MySettings(SettingsFile).GorodValveCommand \
        and not isOnCooldown(MySettings(SettingsFile).GorodValveCommand, data):
            Log("Gorod Valve: called")
            if (GorodValve(data)):
                addCooldown(MySettings(SettingsFile).GorodValveCommand, data)
    return

#============================================================================================================
#   Zombie functions
#============================================================================================================

def Morse(data):
    Log("Morse: called")
    cipher = ""
    paramCount = int(data.GetParamCount())
    letterFound = False

    MORSE_CODE_DICT = { 'A':'.-', 'B':'-...', 
                    'C':'-.-.', 'D':'-..', 'E':'.', 
                    'F':'..-.', 'G':'--.', 'H':'....', 
                    'I':'..', 'J':'.---', 'K':'-.-', 
                    'L':'.-..', 'M':'--', 'N':'-.', 
                    'O':'---', 'P':'.--.', 'Q':'--.-', 
                    'R':'.-.', 'S':'...', 'T':'-', 
                    'U':'..-', 'V':'...-', 'W':'.--', 
                    'X':'-..-', 'Y':'-.--', 'Z':'--..', 
                    '1':'.----', '2':'..---', '3':'...--', 
                    '4':'....-', '5':'.....', '6':'-....', 
                    '7':'--...', '8':'---..', '9':'----.', 
                    '0':'-----', ', ':'--..--', 
                    '?':'..--..', '/':'-..-.', #'?':'..--..', '/':'-..-.', '-':'-....-', 
                    '(':'-.--.', ')':'-.--.-', ' ':'/'}

    for x in range(1,paramCount):
        letterFound = False
        for y in range (0, len(MORSE_CODE_DICT)):
            
            #text to morse
            if data.GetParam(x).lower() == list(MORSE_CODE_DICT.values())[y]:
                letterFound = True
                cipher += list(MORSE_CODE_DICT.keys())[y]
        
            #morse to text
            elif data.GetParam(x).upper() == list(MORSE_CODE_DICT.keys())[y]:
                letterFound = True
                cipher += list(MORSE_CODE_DICT.values())[y]
        
        if not(letterFound):
           cipher += " null "

    SendMessage(cipher)
    return

def GorodValve(data):
    valves = data.GetParam(1).lower()
        
    #Armory
    if valves == "ad":
        SendMessage("A1 S3 T1 I3 C2")
    elif valves == "ac":
        SendMessage("A3 D2 I2 T2 S1")
    elif valves == "ai":
        SendMessage("A2 T2 S1 C2 D2")
    elif valves == "as":
        SendMessage("A2 T1 I1 D3 C1")
    elif valves == "at":
        SendMessage("A3 D2 I3 C1 S3")
    #Dept store
    elif valves == "da":
        SendMessage("D3 C3 I2 T2 S2")
    elif valves == "dc":
        SendMessage("D2 I2 T3 A1 S1")
    elif valves == "di":
        SendMessage("D1 A2 T2 S1 C3")
    elif valves == "ds":
        SendMessage("D1 A2 T1 I3 C1")
    elif valves == "dt":
        SendMessage("D2 I3 C1 S2 A2")
    #Command
    elif valves == "ca":
        SendMessage("C1 S3 T1 I1 D1")
    elif valves == "cd":
        SendMessage("C1 D2 A2 T1 I1")
    elif valves == "ci":
        SendMessage("C1 D3 T3 A3 D2")
    elif valves == "cs":
        SendMessage("C2 D2 I2 T3 A1")
    elif valves == "ct":
        SendMessage("C3 I1 D1 A1 S3")
    #Infirmary
    elif valves == "ia":
        SendMessage("I2 T2 S1 C2 D1")
    elif valves == "id":
        SendMessage("I3 D1 S3 T3 A3")
    elif valves == "ic":
        SendMessage("I2 T2 S2 A3 D3")
    elif valves == "is":
        SendMessage("I3 C2 D1 A2 T2")
    elif valves == "it":
        SendMessage("I3 C2 D1 A1 S3")
    #Supply depot
    elif valves == "sa":
        SendMessage("S3 T1 I3 C2 D1")
    elif valves == "sd":
        SendMessage("S2 A2 T1 I3 C2")
    elif valves == "sc":
        SendMessage("S3 T3 A3 D2 I3")
    elif valves == "si":
        SendMessage("S3 T3 A3 D3 C3")
    elif valves == "st":
        SendMessage("S2 A3 D3 C3 I2")
    #Tank Factory
    elif valves == "ta":
        SendMessage("T1 I1 D3 C1 S2")
    elif valves == "td":
        SendMessage("T1 I3 C1 S2 A3")
    elif valves == "tc":
        SendMessage("T1 I1 D1 A1 S1")
    elif valves == "ti":
        SendMessage("T2 S2 A3 D3 C3")
    elif valves == "ts":
        SendMessage("T1 I3 C2 D1 A1")
    else:
        Log("Gorod Valve: Invalid parameter!")
        SendMessage("Invalid parameter! type 2 combinations of (A D C T S I)")
        return False
    return True